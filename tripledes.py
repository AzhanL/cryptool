#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

from utilities import getChoice, getText, padText, unpadText, getFileWithContent, ocolored


def tripleDESEncryption(key, mode, data):
    """
    Performs TripleDES Encryption with the given key, mode and data

    Mode must be initialized with the nounce or initialization vector

    Returns the encrypted data in bytes
    """
    # Initialize the cipher algorithm
    cipher_algorithm = algorithms.TripleDES(key)
    # Make a cipher object with the given algorithm and mode
    cipher = Cipher(cipher_algorithm, mode, backend=default_backend())
    # Creates an encryptor object
    encryptor = cipher.encryptor()
    # Encrypts the plaintext
    cipher_text = encryptor.update(data) + encryptor.finalize()
    # Returns the cipher text
    return cipher_text


def tripleDESDecryption(key, mode, data):
    """
    Performs TripleDES Decryption with the given key, mode and data

    Mode must be initialized with the nounce or initialization vector

    Returns the plain text in bytes
    """
    # Initialize the cipher algorithm
    cipher_algorithm = algorithms.TripleDES(key)
    # Make a cipher object with the given algorithm and mode
    cipher = Cipher(cipher_algorithm, mode, backend=default_backend())
    # Creates a decryptor object
    decryptor = cipher.decryptor()
    # Decrypts the cipher text
    plaintext = decryptor.update(data) + decryptor.finalize()
    # Returns the plaintext in bytes
    return plaintext


def perform_tripleDES():
    # Gets what the user wants to do (encrypt or decrypt)
    user_choice = getChoice("Would you like to encrypt or decrypt:", ["Encrypt", "Decrypt"])[1]

    # This reads the data from the specified file
    data = getFileWithContent("Enter the file path of your file that needs to be encrypted or that is already encrypted")

    # Generates a nouce or gets one from a file
    iv_nounce = None
    if(user_choice == "Decrypt"):
        iv_nounce = getFileWithContent("Enter the nounce/IV file path below")
    elif(user_choice == "Encrypt"):
        iv_nounce = os.urandom(8)
        print(ocolored(f"Your generated nounce/iv is:{iv_nounce}\n", "yellow"))
        open("nounce_iv.3des", "wb").write(iv_nounce)

    # Gets the keysize from the given options
    keysize = getChoice("Please enter your keysize", [64, 128, 192])[1]

    # Gets the secret key
    print(ocolored("* Note: Your key will be padded if it is not the maximum size", "yellow"))
    key = padText(getText("Please enter your secret key:", 1, int(keysize / 8)), keysize)

    # Prints the padded key for the user
    print(ocolored(f"Your secret key after padding is: {key}\n", "yellow"))

    # Gets the mode the user wants to encrypt/decrypt with
    choice_mode = getChoice("Please enter a mode:", ["CBC", "CTR", "OFB", "CFB"])[1]
    modes_dictionary = {"CBC": modes.CBC(iv_nounce),
                        "CTR": modes.CTR(iv_nounce),
                        "OFB": modes.OFB(iv_nounce),
                        "CFB": modes.CFB(iv_nounce)}
    mode = modes_dictionary[choice_mode]

    # For CBC encryption data needs to be padded
    data = padText(data, 64) if((user_choice == "Encrypt") and (choice_mode == "CBC")) else data

    # Performs encryption or decryption
    output_data = tripleDESEncryption(key, mode, data) if (user_choice == "Encrypt") else tripleDESDecryption(key, mode, data)

    # Unpads the data if necessary
    output_data = unpadText(output_data, 64) if((mode == "CBC") and (user_choice == "Decrypt")) else output_data

    # Outputs the data
    if(user_choice == "Encrypt"):
        print(ocolored(f"\nOutput\n{output_data}\n", "yellow"))
        print(ocolored("Cipher text will be output to encrypted_message.3des", "yellow"))
        open("encrypted_message.3des", "wb").write(output_data)
    elif(user_choice == "Decrypt"):
        print(ocolored(f"\nOutput\n{str(output_data.decode('utf-8'))}\n", "yellow"))
        print(ocolored("Plain text will be output to unencrypted_message.txt", "yellow"))
        open("unencrypted_message.txt", "wb").write(output_data)

    # Asks if the user would like more info on the operation
    more_info = getChoice("\n\nWould you like more info on AES?", ["Yes", "No"])[1]
    if (more_info == "Yes"):
        print("TripleDES? What happened to DES? I’m afraid to said that it simple wasn’t good enough after a while. The original DES (Data Encryption Standard) was developed by the NBS (National Bureau of Statistics) a precursor to NIST (National Institute of Standards and Technology in the 70s. By the later 90s DES was already being broken in under 22 hours, and that was in 1999. Obviously a quick solution was needed. Well take DES and run it three times and voila, TripleDES. Here’s quick rundown of TripleDES, you give it a plaintext value and it divides it into blocks of 8 bytes, and runs it through the TripleDES with one of the keys you give it (key sizes can be 64,128 and,192). Technically out of the 64-bit key only 56 bits are actually used for encryption (the other ones do boring things like parity checks, etc.). DES uses one key, however TripleDES uses the same key three times. Another thing to remember is that TripleDES is backwards compatible with DES.")

        # Prints the info on the mode of operation
        if (mode == "CBC"):
            print("In Cipher Block Chaining(CBC) each block is XORed with the previous. An Initialization Vector(IV) is used at the start only to make each message unique. It is important the IVs are never used twice to avoid  cryptanalsis on cipher texts. This mode of opertion cannot be parralleized since it requires the previous output and input. It operation also requires an encrypted and decryption algorithm.")
        elif (mode == "CTR"):
            print("Counter(CTR) operation requires the use of counters that start at 0 and go to n (depends on the number of blocks). The counter is encrypted with the nouce and the output there is XORed with the plain text to generate the cipher block. Since it does not depend on the previous block for encryption, it can be parallelized for encryption and decryption. It ")
        elif (mode == "OFB"):
            print("During Output Feedback (OFB) the initialization vector(IV) is encrypted and then XORed with the plaintext block. The encrypted IV is also sent back to be encrypted again and then be XORed with the next plaintext block and continues for the rest of the plaintext blocks. Due to it's 'output-input' property, this operation cannot be parallelized. This mode can use the same encryption algorithm for decryption as well.")
        elif (mode == "CFB"):
            print("CFB or Cipher Feedback uses the (as the name implies) the output as input. It first encrypts the initialization vector which then is XORed with the plaintext block to generate a cipher block. This cipherblock is fed in again as input, in replacement of the initialization vector and XORed with the next block of plaintext. Due to it requiring the previous output, it cannot be parrallized but the same encryption algorithm can be utilized for both encryption and decryption.")
    
    # Just pauses
    input("\n\nPress enter to continue")
