#!/bin/python

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from utilities import getChoice, getFileWithContent

def writeFileWithContent(message="Please specify a path ", writeValue=""):
    print(message)
    while True:
        file_path = input("File path: ")
        try:
            # Tries to open the file and write to it
            open(file_path, "wb").write(writeValue)

            return
        # Catches is the file cannot be opened
        except FileNotFoundError as err:
            print("File cannot be opened. Please enter a valid file path and try again")

        # Generic Error
        except Exception as e:
            print("An unknown error occured, please try again")

def perform_rsa():
    
    # Introduction
    print("Rivest-Shamir-Adleman, or RSA, is an asymmetric cryptosystem, meaning that it uses a public-private key pair to encrpyt decrypt data.")
    print("Once data has been encrypted with one key, it can only be decrypted with the other one.")
    print("This module will let you generate a key pair, as well letting you use a public key to encrypt a message, which can then only be decrypted with the corresponding private key.\n\n")
    
    
    #Gets what the user wants to do
    userChoice = getChoice("Would you like to generate a key pair, encrypt, or decrypt:", ["Generate a key pair", "Encrypt", "Decrpyt"])[0]
    
    if(userChoice == 1): # Generate a key
        print("Generating a 2048 bit key pair...\n")
        
        # Generating a 2048 bit private key with a public exponent of 65527, as recommended in the cryptography.io documentation
        privateKey = rsa.generate_private_key(65527, 2048, backend=default_backend())
        publicKey = privateKey.public_key()
        
        print("Key pair generated!\n")
        print("Recommended file extension for keys: .pem\n")
        
        # Writing the private key, unencrypted, to a file
        privateKeyBytes = privateKey.private_bytes(serialization.Encoding.PEM,serialization.PrivateFormat.TraditionalOpenSSL,serialization.NoEncryption())
        writeFileWithContent("Where would you like to save the private key: ", privateKeyBytes)
        
        print("Saved private key!\n")
        
        # Writing the public key to a file
        publicKeyBytes = publicKey.public_bytes(serialization.Encoding.PEM,serialization.PublicFormat.SubjectPublicKeyInfo)
        writeFileWithContent("Where would you like to save the public key: ", publicKeyBytes)
        
        print("Saved public key!\n")
        
        # Prompting the user to give them additional information
        userChoice = getChoice("Would you like to learn more about what happened?", ["Yes", "No"])[1]
        
        if(userChoice == "Yes"):
            
            print("The key generation process involves generating two random prime, p and q,")
            print("then calculating n = pq and Φ(n)=(p - 1)(q - 1).")
            print("Then it chooses a public exponent e, and finds e's multiplicative inverse (mod Φ(n)), d.")
            print("{e, n} becomes the public key, and {d, n} becomes the private key.\n")
            
            publicNums = publicKey.public_numbers()
            privateNums = privateKey.private_numbers()
            print("Here are the numbers that were generated:")
            print("p = {}\nq = {}\nn = {}".format(privateNums.p,privateNums.q,publicNums.n))
            print("Φ(n) = {}".format((privateNums.p - 1) * (privateNums.q - 1)))
            print("e = {}\nd = {}".format(publicNums.e,privateNums.d))
        
    elif(userChoice == 2): # Encrypt
        
        # Reading the public key from a file
        publicKey = getFileWithContent("Enter the file location of the public key: ")
        publicKey = serialization.load_pem_public_key(publicKey, backend=default_backend())
        
        # Reading in the data to be encrypted
        textToEncrypt = getFileWithContent("Which file would you like to encrypt: ")
        try:
            # Encrypting the text using RSA
            ct = publicKey.encrypt(textToEncrypt,padding.OAEP(padding.MGF1(hashes.SHA256()),hashes.SHA256(),None))
            
        except ValueError: # If file exceeds maximum message length
            print("File too large for 2048 bit RSA.")
            input("\n\nPress enter to continue")
            return
        
        # Writing the encrypted text to a file
        cipherFile = open("encryptedMessage.rsa", "wb")
        cipherFile.write(ct)
        cipherFile.close()
        
        print("\nYour file has been encrypted!\n")
        print("Here is your encrypted text:\n{}\n".format(ct))
        print("It has also been saved to encryptedMessage.rsa\n")
        
        # Prompting the user to give them additional information
        userChoice = getChoice("Would you like to learn more about what happened?", ["Yes", "No"])[1]
        
        if(userChoice == "Yes"):
            print("RSA calculates cyphertext as (plaintext)**e mod n")
            print("(for explanations of e and n, see the key generation).\n")
        
    else: # Decrypt
        
        # Reading the private key, unencrypted from a file
        privateKey = getFileWithContent("Enter the file location of the private key")
        privateKey = serialization.load_pem_private_key(privateKey, None,backend=default_backend())
        
        # Reading in the data to be decrypted
        textToDecrypt = getFileWithContent("Which file would you like to decrypt: ")
        
        # Decrypting the text using RSA
        pt = privateKey.decrypt(textToDecrypt,padding.OAEP(padding.MGF1(hashes.SHA256()),hashes.SHA256(),None))
        
        # Writing the decrypted text to a file
        plainFile = open("unencryptedMessage.txt", "wb")
        plainFile.write(pt)
        plainFile.close()
        
        print("\nYour file has been decrypted!\n")
        print("Here is your unencrypted text:\n{}\n".format(pt))
        print("It has also been saved to unencryptedMessage.txt\n")
        
        # Prompting the user to give them additional information
        userChoice = getChoice("Would you like to learn more about what happened?", ["Yes", "No"])[1]
        
        if(userChoice == "Yes"):
            print("RSA calculates plaintext as (cyphertext)**d mod n")
            print("(for explanations of d and n, see the key generation).\n")
    
    # Just pauses
    input("\n\nPress enter to continue")
