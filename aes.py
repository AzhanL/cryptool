#!/usr/bin/env python3

import os

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from utilities import getChoice, getFileWithContent, getText, padText, unpadText, ocolored

def perform_aes():
    # Gets what the user wants to do (encrypt or decrypt)
    user_choice = getChoice("Would you like to encrypt or decrypt:", ["Encrypt", "Decrypt"])[1]

    # This reads the data from the specified file
    data = getFileWithContent("Enter the file path of your file that needs to be encrypted or that is already encrypted")

    # Generates a nouce or gets one from a file
    iv_nounce = None
    if(user_choice == "Decrypt"):
        iv_nounce = getFileWithContent("Enter the nounce/IV file path below")
    elif(user_choice == "Encrypt"):
        iv_nounce = os.urandom(16)
        print(ocolored(f"Your generated nounce/iv is:{iv_nounce}\n", "yellow"))
        open("nounce_iv.aes", "wb").write(iv_nounce)

    # Gets the keysize from the given options
    keysize = getChoice("Please enter your keysize", [128, 192, 256])[1]

    # Gets the secret key
    print(ocolored("* Note: Your key will be padded if it is not the maximum size", "yellow"))
    key = padText(getText("Please enter your secret key:", 1, int(keysize / 8)), keysize)
    # Prints the padded key for the user
    print(ocolored(f"Your secret key after padding is: {key}\n", "yellow"))

    # Gets the mode the user wants to encrypt with
    mode = getChoice("Please enter a mode:", ["CBC", "CTR", "OFB", "CFB"])[1]

    # Sets the algorithm with key
    cipher_algorithm = algorithms.AES(key)

    # This will be overwritten in the follwing code, depending on the modes
    cipher = None

    # Depending on the mode choosen, the necessary steps are taken
    if (mode == "CBC"):
        cipher = Cipher(cipher_algorithm, modes.CBC(iv_nounce), backend=default_backend())
        # Data will be padded if it being encrypted
        if (user_choice == "Encrypt"):
            data = padText(data)
    elif (mode == "CTR"):
        cipher = Cipher(cipher_algorithm, modes.CTR(iv_nounce), backend=default_backend())
    elif (mode == "OFB"):
        cipher = Cipher(cipher_algorithm, modes.OFB(iv_nounce), backend=default_backend())
    elif (mode == "CFB"):
        cipher = Cipher(cipher_algorithm, modes.CFB(iv_nounce), backend=default_backend())

    # Assigns the encryptor or decryptor based on the user choice
    user_task = cipher.encryptor() if (user_choice == "Encrypt") else cipher.decryptor()

    # Performs the needed function (either encryption or decryption)
    output_data = user_task.update(data) + user_task.finalize()

    # Unpads the text if it was padded
    if ((mode == "CBC") and (user_choice == "Decrypt")):
        output_data = unpadText(output_data)

    if(user_choice == "Encrypt"):
        print(ocolored(f"\nOutput\n{output_data}\n", "yellow"))
        print(ocolored("Cipher text will be output to encrypted_message.aes", "yellow"))
        open("encrypted_message.aes", "wb").write(output_data)
    elif(user_choice == "Decrypt"):
        print(ocolored(f"\nOutput\n{str(output_data.decode('utf-8'))}\n", "yellow"))
        print(ocolored("Plain text will be output to unencrypted_message.txt", "yellow"))
        open("unencrypted_message.txt", "wb").write(output_data)

    # Asks if the user wouldl like more info on the operation
    more_info = getChoice("\n\nWould you like more info on AES?", ["Yes", "No"])[1]
    if (more_info == "Yes"):
        print("""
The Advanced Encryption Standard(AES) is a symmetric-key algorithm that was published in 2001 by NIST and is widely adopted and used worldwide.  AES can utilize keys of 128, 192 and 256 bits in size. At a very high level it includes these main steps:
- SubBytes: Which bascially is substitution of bytes according to a predefined table
- ShiftRows: The last 3 rows are shifted left the rest of the rows are left untouched
- MixColums: The 4 bytes in each colums are multiples out by a predefined matrix  
- AddRoundKey: Each bytes is XORed with the round subkey
        """)

        # Prints the info on the mode of operation
        if (mode == "CBC"):
            print("In Cipher Block Chaining(CBC) each block is XORed with the previous. An Initialization Vector(IV) is used at the start only to make each message unique. It is important the IVs are never used twice to avoid  cryptanalsis on cipher texts. This mode of opertion cannot be parralleized since it requires the previous output and input. It operation also requires an encrypted and decryption algorithm.")
        elif (mode == "CTR"):
            print("Counter(CTR) operation requires the use of counters that start at 0 and go to n (depends on the number of blocks). The counter is encrypted with the nouce and the output there is XORed with the plain text to generate the cipher block. Since it does not depend on the previous block for encryption, it can be parallelized for encryption and decryption. It ")
        elif (mode == "OFB"):
            print("During Output Feedback (OFB) the initialization vector(IV) is encrypted and then XORed with the plaintext block. The encrypted IV is also sent back to be encrypted again and then be XORed with the next plaintext block and continues for the rest of the plaintext blocks. Due to it's 'output-input' property, this operation cannot be parallelized. This mode can use the same encryption algorithm for decryption as well.")
        elif (mode == "CFB"):
            print("CFB or Cipher Feedback uses the (as the name implies) the output as input. It first encrypts the initialization vector which then is XORed with the plaintext block to generate a cipher block. This cipherblock is fed in again as input, in replacement of the initialization vector and XORed with the next block of plaintext. Due to it requiring the previous output, it cannot be parrallized but the same encryption algorithm can be utilized for both encryption and decryption.")
    # Just pauses
    input("\n\nPress enter to continue")
