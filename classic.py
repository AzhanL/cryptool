#!/usr/bin/env python3

import os, sys, string
from string import ascii_uppercase
from utilities import getChoice, getText, getPosInt, ocolored

def perform_classic():
    
    user_choice = getChoice("Would you like to use a cipher, or see frequency analysis:", ["Caesar", "Frequency Analysis"])[0]

    if(user_choice == 1):

        print("You have selected to use the classic caesar cipher.")

        plaintext = getText("Please enter your text, between 10 and 1024 characters: ", 10, 1024)
        shift = getPosInt("Please enter the amount to shift by")
        
        ascii = 0
        ciphertext = ""
        
        for char in plaintext:
            if char.isalpha():
                ascii = ord(char)
                ascii += shift
        
                if char.isupper():
                    if ascii > ord('Z'):
                        ascii -= 26
                    elif ascii < ord('A'):
                        ascii += 26
                elif char.islower():
                    if ascii > ord('z'):
                        ascii -= 26
                    elif ascii < ord('a'):
                        ascii += 26
                ciphertext += chr(ascii)
            else:
                ciphertext += char
        
        print('You ciphertext is: {}'.format(ciphertext))
    
    elif (user_choice == 2):
        
        print("You have selected to use frequency analysis. This tool is inexact, and will attempt to make a best guess at cracking you cipher.")

        ciphertext = getText("Please enter your text, between 10 and 1024 characters: ", 10, 1024)

        ciphertext = ciphertext.upper()
        #ciphertext = ciphertext.replace(" ", "")

        cipherlength = len(''.join([i for i in ciphertext if i.isalpha()]))

        letterCount = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0, 'G': 0, 'H': 0, 'I': 0, 'J': 0, 'K': 0, 'L': 0, 'M': 0, 'N': 0, 'O': 0, 'P': 0, 'Q': 0, 'R': 0, 'S': 0, 'T': 0, 'U': 0, 'V': 0, 'W': 0, 'X': 0, 'Y': 0, 'Z': 0}
        
        for char in ascii_uppercase:
            count = ciphertext.count(char)
            #print(count / cipherlength)
            letterCount[char] = count / cipherlength

        print(letterCount)

        searching = 1

        while searching:
            newe = max(letterCount, key=letterCount.get)
        
            print("Attempting a round of decryption. The new largest potential e is: {}".format(newe))
            
            shift = 26 - (ord(newe) - ord("E"))

            ascii = 0
            plaintext = ""
        
            for char in ciphertext:
                if char.isalpha():
                    ascii = ord(char)
                    ascii += shift

                    if ascii > ord('Z'):
                        ascii -= 26
                    elif ascii < ord('A'):
                        ascii += 26
                    plaintext += chr(ascii)
                else:
                    plaintext += char
        
            print('You ciphertext might be: {}'.format(plaintext))

            searching = getPosInt("Please enter 0 to end, any integer to keep going")
            
            letterCount[newe] = 0.0

    if getPosInt("Please enter any positive number for extra information, 0 to continue"):
        print("Each letter is assigned a frequency, the times it appears in the text divided by the total number of alphabetical characters. The solver assumes the most common letter is a potential 'E', and attempts t osolve the cipher as such. Each time a potential 'E' is passed, its frequency is set to zero. When all frequencies have been tried, if none were correct the solver will keep recycling 'A' until the user quits. This solver is more accurate on larger bodies of text, unless e is for some reason rarely used.")

    input("\n\nPress enter to continue")
    
if __name__ == '__main__':
    perform_classic()
