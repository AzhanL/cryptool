#!/usr/bin/env python3

import sys

from cryptography.hazmat.primitives import padding
from termcolor import colored


def ocolored(message="Test Message", color="red"):
    # Uses colors on linux
    if (sys.platform.lower() == "linux"):
        return colored(message, color)
    else:
        return message


def getChoice(message="Please enter an option:", choices=[1, 2, 3]):
    """
    Returns a list with the choice position and the choice itself in the following format
    [choice_position, choice]

    message = is the user message in string ex. "Please enter an option"

    choices = is a list of the predefined choices ex. [128, 192, 256]

    Example Usage:

    getChoice("Please choose a keysize:", [128,192, 256])

    getChoice("Please choose a color:", ['red', 'blue', 'green'])
    """
    print(message, "\n")
    # Prints out the options. NOTE: Enumerate returns the index, value
    for i, single_choice in enumerate(choices):
        print(f" {i+1: >3}) {single_choice}")
    print("")

    # Takes input until a valid input is given
    while True:
        try:
            # Tries to take an input
            user_option = int(input(f"[1-{len(choices)}]> "))
            # Checks if it is a valid input, then returns it
            if (0 < user_option <= len(choices)):
                return [user_option, choices[user_option - 1]]
            # Otherwise raises an exception which force it to take the input again
            else:
                print(ocolored("Sorry, not an option", "red"))
                raise ValueError("OUT OF RANGE")
        # Does not do anything with the exceptions just asks for the user to enter again
        except Exception as e:
            pass


def getText(message="Please enter your text: ", min_length=0, max_length=32):
    """
    Returns a string which is within the given length

    Example usage:

    getText("Please enter your name", 1, 32)

    """
    print(message, "\n")

    while True:
        # Take in user input
        user_input = input(f"[{min_length}-{max_length} characters]> ")
        # Checks if the user input is within the size limit
        if (min_length <= len(user_input) <= max_length):
            return user_input
        else:
            print(ocolored("Invalid input, please try again", "red"))


def padText(text="", bits=128):
    padder = padding.PKCS7(bits).padder()

    # Uses the byte format if it is already in bytes
    if (type(text) == bytes):
        return (padder.update(text) + padder.finalize())
    else:
        return (padder.update(text.encode("utf-8")) + padder.finalize())


def unpadText(text="", bits=128):
    unpadder = padding.PKCS7(bits).unpadder()

    # Uses the byte format if it is already in bytes
    if (type(text) == bytes):
        return (unpadder.update(text) + unpadder.finalize())
    else:
        return (unpadder.update(text.encode("utf-8")) + unpadder.finalize())


def getFileWithContent(message="Please specify a path"):
    print(message)
    while True:
        file_path = input("File path: ")
        try:
            # Tries to open the file
            data = open(file_path, "rb").read()

            # If no errors occurs then it will return the contents of the file
            return data

        # Catches is the file cannot be opened
        except FileNotFoundError as err:
            print(ocolored("File cannot be opened. Please enter a valid file path and try again"), "red")

        # Generic Error
        except Exception as e:
            print(ocolored("An unknown error occured, please try again", "red"))

def getPosInt(message="Please enter your integer: "):
    print(message, "\n")
    while True:
        user_input = input(f"> ")
        try:
            num = int(user_input)
            return abs(num)
        except ValueError:
            print("Please only enter valid integers")
