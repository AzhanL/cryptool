#!/usr/bin/env python3
import shutil


import utilities
from utilities import ocolored
from aes import perform_aes
from tripledes import perform_tripleDES
from randomNums import perform_random
from rsa import perform_rsa
from classic import perform_classic


def printFile(file="art/logo.txt", center=False, color=""):
    """
    Prints the contents of the file and centers it if user requires
    """
    # Gets the size of terminal
    columns = shutil.get_terminal_size().columns
    # lines = shutil.get_terminal_size().lines

    # Opens the art file
    art = open('art/logo.txt').readlines()

    # Prints line by line because centering of independant lines may be needed
    for line in art:
        # Color in the text if required
        line_print = ocolored(line.replace('\n', ''), color) if (color.isalpha()) else line.replace('\n', '')
        if center:
            print(f"{line_print: ^{columns}}")
        else:
            print(f"{line_print: <{columns}}")


def main_menu():
    while True:
        # Prints the default (logo)
        printFile(center=True, color="yellow")
        # Retreives what the user would like to do
        option = utilities.getChoice("Please enter an option", ["AES", "TripleDES", "RSA", "Pseudo Random Numbers", "Classic Ciphers", "Exit"])[1]
        print("")

        # Based on the option perform the asked task
        if (option == "AES"):
            perform_aes()
        elif (option == "TripleDES"):
            perform_tripleDES()
        elif (option == "RSA"):
            perform_rsa()
        elif (option == "Pseudo Random Numbers"):
            perform_random()
        elif (option == "Classic Ciphers"):
            perform_classic()
        elif (option == "Exit"):
            exit(0)


if __name__ == "__main__":
    main_menu()
